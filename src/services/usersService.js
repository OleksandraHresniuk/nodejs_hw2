const bcrypt = require('bcrypt')
const { User } = require('../models/userModel')
const { Credential } = require('../models/credentialModel')

const getUserInfo = async (username) => {
  const user = await User.findOne({username})
  return user
}

const deleteUser = async (username) => {
  await User.deleteOne({username})
  await Credential.deleteOne({username})
}

const editUser = async (username, newPassword) => {
  const user = await Credential.findOne({username})
  const oldPassword = user.password
  await Credential.updateOne({ username: username}, {password: await bcrypt.hash(newPassword, 10)})
  return oldPassword
}

module.exports = {
  getUserInfo,
  deleteUser,
  editUser
}
