const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')

const { Credential } = require('../models/credentialModel')
const { User } = require('../models/userModel')

const registration = async ({username, password}) => {
  const credential = new Credential({
    username,
    password: await bcrypt.hash(password, 10)
  })
  await credential.save()

  const user = new User({
    username
  })
  await user.save()
}

const login = async ({username, password}) => {
  const credential = await Credential.findOne({username})
  const user = await User.findOne({username})

  if (!credential) {
    throw new Error('Invalid username or password')
  }

  if (!(await bcrypt.compare(password, credential.password))) {
    throw new Error('Invalid username or password')
  }

  const token = jwt.sign({
    _id: user._id,
    username: user.username
  }, 'secret')
  return token
}

module.exports = {
  registration,
  login
}
