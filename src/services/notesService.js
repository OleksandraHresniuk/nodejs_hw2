const { Note } = require('../models/noteModel')

const getNotesByUserId = async (userId, offset, limit) => {
  const notes = await Note.find({ userId })
    .skip(+offset)
    .limit(+limit)
  
  return notes
}

const getNotesCountByUserId = async (userId) => {
  const count = await Note.countDocuments({ userId })
  return count
}

const addNoteToUser = async (userId, notePayload) => {
  const note = new Note({ ...notePayload, userId })
  await note.save()
}

const getNoteByNoteId = async (noteId, userId) => {
  const note = await Note.findOne({ _id: noteId, userId })

  return note
}

const updateNoteByNoteId = async (noteId, userId, text) => {
  const note = await Note.findOneAndUpdate({ _id: noteId, userId }, {$set: text})
}

const changeStatusByNoteId = async (noteId, userId, boo) => {
  const note = await Note.findOneAndUpdate({ _id: noteId, userId }, {completed: boo})
}

const deleteNoteByNoteId = async (noteId, userId) => {
  const note = await Note.findOneAndRemove({ _id: noteId, userId })
}

module.exports = {
  getNotesByUserId,
  getNotesCountByUserId,
  addNoteToUser,
  getNoteByNoteId,
  updateNoteByNoteId,
  changeStatusByNoteId,
  deleteNoteByNoteId
}
