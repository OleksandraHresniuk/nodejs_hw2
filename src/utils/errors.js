class ApiError extends Error {
  constructor (message) {
    super(message)
    this.status = 500
  }
}

class InvalidRequestError extends ApiError{
  constructor (message = 'Invalid request') {
    super(message)
    this.status = 400
  }
}

module.exports = {
  ApiError,
  InvalidRequestError
}