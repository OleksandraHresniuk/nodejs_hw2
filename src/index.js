const express = require('express')
const mongoose = require('mongoose')
const path = require('path')
const morgan = require('morgan')
require('dotenv').config()

const port = process.env.PORT
const host = process.env.HOST
const db_host = process.env.DB_HOST
const db_user = process.env.DB_USER
const db_pass = process.env.DB_PASS

const app = express()

const { notesRouter } = require('./controllers/notesController');
const { usersRouter } = require('./controllers/usersController');
const { authRouter } = require('./controllers/authController');
const { authMiddleware } = require('./middlewares/authMiddleware');
const { ApiError } = require('./utils/errors');

app.use(express.json());
app.use(morgan('tiny'));
app.use(express.urlencoded({ extended: true }))

app.use('/api/auth', authRouter);
app.use('/api/users/me', [authMiddleware], usersRouter);
app.use('/api/notes', [authMiddleware], notesRouter);

app.use((req, res, next) => {
  res.status(404).json({ message: 'Not found' })
})

app.use((err, req, res, next) => {
  if (err instanceof ApiError) {
    return res.status(err.status).json({ message: err.message })
  }
  res.status(500).json({ message: err.message })
})

const start = async () => {
  try {
    await mongoose.connect(`mongodb+srv://${db_user}:${db_pass}${db_host}`, { useNewUrlParser: true, useUnifiedTopology: true })
    app.listen(port, host, () => {
      console.log('Server has been started...');
    })
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
}

start()
