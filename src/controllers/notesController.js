const express = require('express');
const router = express.Router();

const { getNotesByUserId,
    getNotesCountByUserId,
    addNoteToUser,
    getNoteByNoteId,
    updateNoteByNoteId,
    changeStatusByNoteId,
    deleteNoteByNoteId
} = require('../services/notesService')

const { asyncWrapepr } = require('../utils/apiUtils')
const { InvalidRequestError } = require('../utils/errors')

router.get('/', asyncWrapepr(async (req, res) => {
    const { userId } = req.user

    const offset = req.query.offset === undefined ? 0 : req.query.offset
    const limit = req.query.limit === undefined ? 0 : req.query.limit
    const count = await getNotesCountByUserId(userId)

    const notes = await getNotesByUserId(userId, offset, limit)

    res.status(200).json({
        offset: offset,
        limit: limit,
        count: count,
        notes
    })
}))

router.post('/', asyncWrapepr(async (req, res) => {
    const { userId } = req.user

    await addNoteToUser(userId, req.body)

    res.status(200).json({ message: 'Success' })
}))

router.get('/:id', asyncWrapepr(async (req, res) => {
    const { userId } = req.user
    const { id } = req.params

    const note = await getNoteByNoteId(id, userId)

    if (!note) {
        throw new InvalidRequestError('Note with such id not found!')
    }

    res.status(200).json({ note })
}))

router.put('/:id', asyncWrapepr(async (req, res) => {
    const { userId } = req.user
    const { id } = req.params
    const text = req.body

    await updateNoteByNoteId(id, userId, text)
    res.status(200).json({ message: "Success" })
}))

router.patch('/:id', asyncWrapepr(async (req, res) => {
    const { userId } = req.user
    const { id } = req.params

    const note = await getNoteByNoteId(id, userId)
    
    const boo = note.completed === true ? false : true
   
    await changeStatusByNoteId(id, userId, boo)
    res.status(200).json({ message: "Success" })
}))

router.delete('/:id', asyncWrapepr(async (req, res) => {
    const { userId } = req.user
    const { id } = req.params

    await deleteNoteByNoteId(id, userId)
    res.status(200).json({ message: "Success" })
}))

module.exports = {
    notesRouter: router
}