const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt')

const { getUserInfo, deleteUser, editUser } = require('../services/usersService')
const { asyncWrapepr } = require('../utils/apiUtils')

router.get('/', asyncWrapepr(async (req, res) => {
  const { username } = req.user

  const user = await getUserInfo(username)

  res.status(200).json({ user })
}))

router.delete('/', asyncWrapepr(async (req, res) => {
  const { username } = req.user

  await deleteUser(username)

  res.status(200).json({ message: "Success" })
}))

router.patch('/', asyncWrapepr(async (req, res) => {
  const { username } = req.user

  const newPassword = req.body.newPassword
  
  if (newPassword === undefined) {
    res.status(400).json({ message: 'Provide new password' })
  } else {
    const oldPassword = await editUser(username, newPassword)
    res.status(200).json({ oldPassword: oldPassword, newPassword: await bcrypt.hash(newPassword, 10) })
  }
}))

module.exports = {
  usersRouter: router
}
