const express = require('express');
const router = express.Router();

const { registration, login } = require('../services/authService')
const { asyncWrapepr } = require('../utils/apiUtils')


router.post('/register', asyncWrapepr(async (req, res) => {

  const {
    username,
    password
  } = req.body

  await registration({ username, password })

  res.json({ message: 'Success' })
}))

router.post('/login', asyncWrapepr(async (req, res) => {

  const {
    username,
    password
  } = req.body

  const jwt_token = await login({ username, password })

  res.json({ message: 'Success', jwt_token })
}))

module.exports = {
  authRouter: router
}